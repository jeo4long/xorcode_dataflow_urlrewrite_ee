<?php
class Xorcode_Dataflow_Urlrewrite_Adapter
    extends Mage_Eav_Model_Convert_Adapter_Entity
{
    protected $_urlCache = array();

    protected $_stores;

    public function parse ()
    {
        $batchModel = Mage::getSingleton('dataflow/batch');
        $batchImportModel = $batchModel->getBatchImportModel();
        $importIds = $batchImportModel->getIdCollection();

        foreach ($importIds as $importId) {
            $batchImportModel->load($importId);
            $importData = $batchImportModel->getBatchData();
            $this->saveRow($importData);
        }
    }

    /**
     * Save urlRewrite (import)
     *
     * @param array $importData
     * @throws Mage_Core_Exception
     * @return bool
     */
    public function saveRow (array $importData)
    {
        if (empty($importData['store'])) {
            if (!is_null($this->getBatchParams('store'))) {
                $store = $this->getStoreByCode($this->getBatchParams('store'));
            } else {
                $message = Mage::helper('catalog')->__('Skip import row, required field "%s" not defined', 'store');
                Mage::throwException($message);
            }
        } else {
            $store = $this->getStoreByCode($importData['store']);
        }

        if ($store === false) {
            $message = Mage::helper('catalog')->__('Skip import row, store does not exist');
            Mage::throwException($message);
        }

        $options = null;

        if (empty($importData['options'])) {
            if (!is_null($this->getBatchParams('options'))) {
                $options = $this->getBatchParams('options');
            }
        }

        $options = strtoupper($options);
        switch ($options) {
            case 'R':
            case 'RP':
            case '':
                break;
            default:
                $options = '';
                break;
        }

        // Strip leading slash, if any
        $importData['target_path'] = preg_replace('/^\//', '', $importData['target_path']);
        $importData['request_path'] = preg_replace('/^\//', '', $importData['request_path']);

        $url = Mage::getModel('core/url_rewrite')
            ->setIdPath($importData['request_path'])
            ->setIsSystem(0)
            ->setOptions($options)
            ->setStoreId($store->getId())
            ->setTargetPath($importData['target_path'])
            ->setRequestPath($importData['request_path'])
            ->save();

        return true;
    }

    /**
     * Retrieve store object by code
     *
     * @param string $store
     * @return Mage_Core_Model_Store
     */
    public function getStoreByCode ($store)
    {
        $this->_initStores();
        if (isset($this->_stores[$store])) {
            return $this->_stores[$store];
        }
        return false;
    }

    /**
     *  Init stores
     *
     *  @param    none
     *  @return      void
     */
    protected function _initStores ()
    {
        if (is_null($this->_stores)) {
            $this->_stores = Mage::app()->getStores(true, true);
            foreach ($this->_stores as $code => $store) {
                $this->_storesIdCode[$store->getId()] = $code;
            }
        }
    }
}

